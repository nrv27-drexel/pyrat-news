def bitest(url):
    """
    Returns True if url is in fake_sites.txt
    """
    infile = open('fake_sites.txt', 'r')
    for line in infile:
        if line.rstrip() in url:
            infile.close()
            return True
    infile.close()
    return False

def tritest(url):
    """
    Returns 0 if the domain is on blacklist,
    1 if the domain is on whitelist, 2 if the
    domain is on neither of the lists
    """
    blacklist = open('fake_sites.txt', 'r')
    for line in blacklist:
        if line.rstrip() in url:
            blacklist.close()
            return 0
    blacklist.close()
    whitelist = open('real_sites.txt', 'r')
    for line in whitelist:
        if line.rstrip() in url:
            whitelist.close()
            return 1
    whitelist.close()
    return 2