import sys
from PyQt5.QtWidgets import QWidget, QApplication, QInputDialog, QDialog, QLineEdit, QPushButton, QSizePolicy, QTextEdit, QHBoxLayout, QVBoxLayout
from PyQt5.QtGui import QPainter, QColor, QFont
from PyQt5.QtCore import Qt

from get_article import art
import Processer

class Results(QDialog):

    def __init__(self):
        super().__init__()

        # Setting window size and title
        self.left = 10
        self.top = 10
        self.width = 800
        self.height = 600
        self.title = "Output"

        # Obtain the results from the backend
        self.authorCred = "harambe"
        self.publisherCred = "$$$"
        self.topicCred = "90"
        self.totalCred = "324"

        # Hold the string outputs
        self.authorString = ""
        self.publisherString = ""
        self.topicString = ""
        self.totalString = ""
        

        # Get URl from the user
        self.getURL()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # Determine the text to display and display it
        #self.determineStrOutput()

        self.text1 = QTextEdit()
        self.text1.setReadOnly(True)
        self.text1.setFontPointSize(24)
        self.text1.append(self.authorString)
        self.text1.append(self.publisherString)
        self.text1.append(self.topicString)
        self.text1.append(self.totalString)

        #hbox = QHBoxLayout()
        vbox = QVBoxLayout()
        vbox.addWidget(self.text1)
        self.setLayout(vbox)

        self.show()
        """
    def determineStrOutput(self):
        self.authorString = "Author's Credability: <b>{}%</b>\n".format(str(self.authorCred))
        self.publisherString = "Publisher's Credability: <b>{}%</b>\n".format(str(self.publisherCred))
        self.topicString = "Topic Credability: <b>{}%</b>\n".format(str(self.topicCred))
        self.totalString = "<b>Total Credability: {}%\n".format(str(self.totalCred))
        """
    def getURL(self):
        dlg = QInputDialog(self)
        dlg.setInputMode( QInputDialog.TextInput)
        dlg.setLabelText("URL:")
        dlg.resize(500,100)
        ok = dlg.exec_()
        url = dlg.textValue()

        if ok and url != "":
            #print(url)
            # process url
            newArticle = art(url)
            data = newArticle.getAttribs()
            self.authorCred = data[0]
            self.publisherCred = data[1]
            self.topicCred = data[2]
            rating = Processer.query(data[0], data[1], data[2])
            types = [0, 0, 0]
            if type(rating[0]) == str:
                self.authorString = rating[0]
                types[0] = 1
                rating[0] = 0
            else:
                self.authorString = "Author's Credability: <b>{}%</b>\n".format(str(self.authorCred))
            if type(rating[1]) == str:
                self.publisherString = rating[1]
                types[1] = 1
                rating[1] = 0
            else:
                self.publisherString = "Publisher's Credability: <b>{}%</b>\n".format(str(self.publisherCred))
            if type(rating[2]) == str:
                self.topicString = rating[2]
                types[2] = 1
                rating[2] = 0
            else:
                self.topicString = "Topic Credability: <b>{}%</b>\n".format(str(self.topicCred))

            articleFinal = 0
            if 1 not in types:
                articleFinal = sum(rating)/3

            if sum(types) == 1:
                articleFinal = sum(rating)/2
            
            
            elif sum(types) >= 2:
                articleFinal = "Our database does not have enough information to accuratly calculate credibility for this article."
            
            if type(articleFinal) == int:
                self.totalString = "Total summary for this article is " + str(articleFinal) + "% "
                Processer.updateDatabase(articleFinal, types, data)
            else:
                self.totalString = articleFinal
            


            # Show UI
            self.initUI()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Results()
    sys.exit(app.exec_())
