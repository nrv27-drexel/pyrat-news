from difflib import SequenceMatcher

def similar(a,b):
    return SequenceMatcher(None, a,b).ratio()
			   
			   
a = similar('Staff just happened', 'staff just happened')
b = similar('Staff just happened', 'happened staff just')
c = similar('Staff just happened', 'staff did not happen')
print(a)
print(b)
print(c)
