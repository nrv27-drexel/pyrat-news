from get_article import art
import sqlite3

def evalCitations(cursor, in_article, rate):
    """ if an article cites sources, grabs ratings for the sources from the db"""
    stats = []
    db_cites = [x for x in cursor.find("all publishers")]
    #each db_cite has structure (name, rating, count)
    for cite in in_article.cites():
        for i in range(db_cites):
            if db_cites[i][0] == cite:
                stats.append((db_cites[i][1],(rate)*db_cites[i][2]))
    #returns a list of tuples (ratings, adjusted_count)
    return stats

# Functions for converting 1-100% values to values stored in database, and vise versa
def PercToDec(n):
    return (n-50)/100

def DecToPerc(n):
    return n*100 + 50


def query(authorNames, publisherName, topicName):
    """ query the article database for author, publisher, and topic information """
    topicName = topicName.replace('\'', '\"')
    conn = sqlite3.connect("pyrat_news.sqlt")
    c = conn.cursor()

    # handle articles with multiple authors
    if (len(authorNames) != 0):
        Arating = processAuthors(authorNames, c)
    else:
        Arating = "No author"
    
    Prating = Trating = 0

    # query for publishers
    c.execute(""" SELECT `publisherRating`
    FROM `publishers` WHERE `publisherName`='%s';""" % publisherName)
    row = c.fetchone()
    if (row):
        Prating = row[0]
    else:
        Prating = "Publisher is not yet known"
    
    # query for topics
    c.execute("""SELECT `topicCount`, `topicRating`
FROM `topics` WHERE `topicName`='%s';""" % topicName)
    row = c.fetchone()
    if (row):
        Trating = row[1]
    else:
        Trating = "Unkown topic"
    
    # return a list, said list will contain exactly 3 elements
    # ideally, all three elements will contain a percentage value
    # however, if we are unable to obtain a percentage value from the database, said values will be an error string
    return [Arating, Prating, Trating]


def processAuthors(authorNames, sql_cursor):
    """ used for handling articles with multiple authors"""
    rating = 0
    count = 0
    
    for author in authorNames:
        author = author.replace('\'', "\"")
        sql_cursor.execute("""SELECT `topicCount`, `topicRating`
FROM `topics` WHERE `topicName`='%s';""" % author)
        row = sql_cursor.fetchone()
        if (row):
            count += (row[0])
            rating += (row[0] * row[1])
	
    if (count == 0):
        # return an error string if no authors existed in the database
        return "No known authors"
    else:
        # return a wieghted average rating of all the authors of the article that existed in the database
        return rating/count
    
def updateDatabase(final, types, data):
    """ given a final percentage rating for an article, update the ratings in the database related to said article"""
    conn = sqlite3.connect("pyrat_news.sqlt")
    c = conn.cursor() 
    articleInfo = publisherInfo = topicInfo = 0
    if len(data[0]) != 0:
        author = data[0][0]

        if types[0] == 0:
            c.execute("""SELECT `authorCount`, `authorRating`
	    FROM `authors` WHERE `authorName`='%s';""" % author)
            articleInfo = c.fetchone()
    publisher = data[1]
    if types[1] == 0:
        c.execute("""SELECT `publisherCount`, `publisherRating` FROM `publishers` WHERE `publisherName` = '%s';""" % publisher)
        publisherInfo = c.fetchone()
    topic = data[2]
    if types[2] == 0:
        c.execute("""SELECT `topicCount`, `topicRating` FROM `topic` WHERE `topicName`='%s';""" % topic)
        topicInfo = c.fetchone()

    print(publisherInfo)
    
