from newspaper import Article
from bs4 import BeautifulSoup
import urllib3

class art:
    def __init__(self, url):
        a = Article(url)
        a.download()
        a.parse()
        self.url = url
        self.author = a.authors
        self.title = a.title
        if url.startswith("https"):
            self.publisher = url[len("https://"):]
        else:
            self.publisher = url[len("http://"):]
        self.publisher = self.publisher[:self.publisher.find("/")]
        self.citations = self.find_citations(url)

    def find_citations(self, url):
        """
        Returns a list of all links on a webpage minus certain webpages
        that appear in blacklist.txt and minus and links that share the
        same root as the url(parameter)
        """
        http = urllib3.PoolManager()

        response = http.request('GET',url)
        soup = BeautifulSoup(response.data, 'lxml')

        #Make a list of all the blacklister sites from blacklist.txt
        infile = open('blacklist.txt', 'r')
        blacklist = [x.rstrip() for x in infile]
        infile.close()
        listCites = []

        #loop through every link on the webpage. if the link is not a link
        #back to root and the link isn't on the blacklist, add the link to
        #a list
        for link in soup.find_all('a'):
            linkName = str(link.get('href'))
            if 'http' in linkName and not self.publisher in linkName:
                listCites.append(linkName)
                for item in blacklist:
                    if item in linkName:
                        listCites.remove(linkName)
        return listCites

    def getAttribs(self):
        return [self.author, self.publisher, self.title]

    def getCitations(self):
        return self.citations
