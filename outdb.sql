--CREATE DATABASE  IF NOT EXISTS `pyrat_news`
--USE `pyrat_news`;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;

CREATE TABLE `authors` (
  `authorID`		INTEGER 	AUTO_INCREMENT,
  `authorName`		varchar(31)	DEFAULT NULL,
  `authorRating`	DECIMAL(3,2)	DEFAULT '0',
  `authorCount`		int(11)		DEFAULT '0',
  CONSTRAINT pk_AuthorID 		PRIMARY KEY	(`authorID`),
  CONSTRAINT uc_AuthorName 		UNIQUE		(`authorName`),
  CONSTRAINT chk_AuthorRating 		CHECK 		(`authorRating` >= (-1) AND `authorRating` <= 1)
);



--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  articleID		INTEGER		AUTO_INCREMENT,
  `articleRating`	DECIMAL(3,2)	DEFAULT '0',
  CONSTRAINT pk_ArticleID 		PRIMARY KEY	(`articleID`),
  CONSTRAINT chk_ArticleRating 		CHECK		(`articleRating` >= (-1) AND `articleRating` <= 1)
);


--
-- Table structure for table `publishers`
--

DROP TABLE IF EXISTS `publishers`;

CREATE TABLE `publishers` (
  `publisherID`		INTEGER		AUTO_INCREMENT,
  `publisherName`	varchar(255)	DEFAULT NULL,
  `publisherRating`	DECIMAL(3,2)	DEFAULT '0',
  `publisherCount`	int(11)		DEFAULT '0',
  CONSTRAINT pk_PublisherID 		PRIMARY KEY	(`publisherID`),
  CONSTRAINT uc_PublisherName 		UNIQUE		(`publisherName`),
  CONSTRAINT chk_PublisherRating 	CHECK		(`publisherRating` >= (-1) AND `publisherRating` <= 1)
);



--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;

CREATE TABLE `topics` (
  `topicID`		INTEGER		AUTO_INCREMENT,
  `topicName`		varchar(255)	DEFAULT NULL,
  `topicRating`		DECIMAL(2,2)	DEFAULT '0',
  `topicCount`		int(11)		DEFAULT '0',
  CONSTRAINT pk_TopicID 		PRIMARY KEY 	(`topicID`),
  CONSTRAINT uc_TopicName 		UNIQUE		(`topicName`),
  CONSTRAINT chk_TopicRating 		CHECK 		(`topicRating` >= (-1) AND `topicRating` <= 1)
);


--
-- Table structure for table `author_article`
--

DROP TABLE IF EXISTS `author_article`;

CREATE TABLE `author_article` (
  `authorID`		int(11)		NOT NULL DEFAULT '0',
  `articleID`		int(11) 	NOT NULL DEFAULT '0',
  CONSTRAINT uc_AuthorArticle 		UNIQUE		(`authorID`,`articleID`),
  CONSTRAINT pk_AuthorArticle 		PRIMARY KEY 	(`authorID`,`articleID`),
  
  CONSTRAINT fk_AuthorArticle_Author		FOREIGN KEY (`authorID`)
  REFERENCES authors(`authorID`),
  CONSTRAINT fk_AuthorArticle_Article		FOREIGN KEY (`articleID`)
  REFERENCES articles(`articleID`)
);


--
-- Table structure for table `publisher_article`
--

DROP TABLE IF EXISTS `publisher_article`;

CREATE TABLE `publisher_article` (
  `publisherID`		int(11)		NOT NULL DEFAULT '0',
  `articleID`		int(11)		NOT NULL DEFAULT '0',
  CONSTRAINT uc_PublisherArticle 	UNIQUE		(`articleID`),
  CONSTRAINT pk_PublisherArticle 	PRIMARY KEY 	(`publisherID`,`articleID`),
  
  CONSTRAINT fk_PublisherArticle_Publisher	FOREIGN KEY (`publisherID`)
  REFERENCES publishers(`publisherID`),
  CONSTRAINT fk_PublisherArticle_Article	FOREIGN KEY (`articleID`)
  REFERENCES articles(`articleID`)
);


--
-- Table structure for table `topic_article`
--

DROP TABLE IF EXISTS `topic_article`;

CREATE TABLE `topic_article` (
  `topicID` 		int(11)		NOT NULL DEFAULT '0',
  `articleID` 		int(11)		NOT NULL DEFAULT '0',
  CONSTRAINT uc_TopicArticle 		UNIQUE		(`articleID`),
  CONSTRAINT pk_TopicArticle 		PRIMARY KEY 	(`topicID`,`articleID`),
  
  CONSTRAINT fk_TopicArticle_Topic		FOREIGN KEY (`topicID`)
  REFERENCES topics(`topicID`),
  CONSTRAINT fk_TopicArticle_Article		FOREIGN KEY (`articleID`)
  REFERENCES articles(`articleID`)
);
